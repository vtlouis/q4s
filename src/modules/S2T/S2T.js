import React, { useEffect, useState } from 'react';

import SpeechRecognition, { useSpeechRecognition } from 'react-speech-recognition'

const S2T = () => {

    const [isListening, setListening] = useState(false);

    const {
        transcript,
        resetTranscript,
        browserSupportsSpeechRecognition
    } = useSpeechRecognition();

    if (!browserSupportsSpeechRecognition) {
        return <span>Browser doesn't support speech recognition.</span>;
    }

    useEffect(() => {
        if (isListening) {
            SpeechRecognition.startListening({ continuous: true, language: 'en-US' });
            console.log("listening start")
        }

    }, [isListening])

    return (
        <div>
            <form>
                <button onClick={() => setListening(true)}>Start</button>
                <button onClick={() => setListening(false)}>Start</button>
                <textarea value={transcript}></textarea>
                <button onClick={resetTranscript}>Reset</button>
            </form>
        </div>
    )
}

export default S2T