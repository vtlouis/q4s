import React, { useState } from "react";
import useSound from 'use-sound';


const LoadingStart = () => {

    const [soundEnd, setSoundEnd] = useState(false);

    const [playEffect, exposedData] = useSound(
        '/media/mp3/StartGame.mp3',
        { volume: 0.25 }
    );

    if (!soundEnd) playEffect();

    if (exposedData.duration) {

        setTimeout(() => {
            setSoundEnd(true);
            exposedData.stop();
        }, exposedData.duration + 1000)
    }

    if (soundEnd) return <></>;

    return (
        <div className="w-full h-screen fixed bg-black bg-opacity-65 backdrop-blur-background z-1">
            <div className="absolute top-1/2 left-1/2 transform -translate-x-1/2 -translate-y-1/2 z-2">
                <div className='flex items-center bg-clip-text bg-logo'>
                    <h3 className='text-28 text-transparent font-bold tracking-tighter'>Quiz</h3>
                    <div className='w-[30px] h-[30px]'>
                        <img src='/images/icons/flash.png' alt='' />
                    </div>
                    <h3 className='text-28 text-transparent font-bold tracking-tighter'>Speed</h3>
                </div>
                <div className="flex justify-center pt-4 pb-8">
                    <h3 className='text-32 text-white font-black '>Start!</h3>
                </div>
                <div className=" flex justify-center">
                    <div className='w-[100px] h-[100px]'>
                        <img src='/images/icons/coin_dollar.png' alt='' />
                    </div>
                </div>
            </div>
        </div>
    )
}

export default LoadingStart;