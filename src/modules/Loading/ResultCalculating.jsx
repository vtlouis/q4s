import React, { useEffect, useState } from "react";
import useSound from 'use-sound';
import ResultSummaries from "../Result/ResultSummaries";


const ResultCalculating = ({ calculating, resultSummary }) => {

    const [soundEnd, setSoundEnd] = useState(false);

    const [playEffect, exposedData] = useSound(
        '/media/mp3/Calculating.mp3',
        { volume: 0.25 }
    );


    useEffect(() => {
        if (calculating) playEffect();

        if (exposedData.duration && calculating) {
            setTimeout(() => {
                setSoundEnd(true);
                exposedData.stop();
            }, exposedData.duration + 2000)
        }

    }, [
        calculating,
        exposedData,
        playEffect
    ])


    if (soundEnd && calculating) return <ResultSummaries resultSummary={resultSummary} />;


    if (!calculating) {
        return <></>;
    }
    else
        return (
            <div className="w-full h-screen fixed bg-black bg-opacity-65 backdrop-blur-background z-1">
                <div className="absolute top-1/2 left-1/2 transform -translate-x-1/2 -translate-y-1/2 z-2">

                    <div className=" flex justify-center">
                        <div className='w-[100px] h-[100px]'>
                            <img src='/images/icons/message.png' alt='' />
                        </div>
                    </div>

                    <div className="flex justify-center pt-4 pb-8">
                        <h3 className='text-28 text-white font-black'>정산 중..</h3>
                    </div>
                </div>
            </div>
        )


}

export default ResultCalculating;