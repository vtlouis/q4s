import ProgressBar from "@ramonak/react-progress-bar";
import React, { useEffect, useRef, useState } from "react";
import ReactPlayer from "react-player";
import { Splide, SplideSlide, SplideTrack } from '@splidejs/react-splide';

import '@splidejs/react-splide/css/core';
import { Link } from "react-router-dom";
import LoadingStart from "../Loading/LoadingStart";

import useSound from 'use-sound';
import { useSpeechSynthesis } from 'react-speech-kit';

import ResultCalculating from "../Loading/ResultCalculating";

import SpeechRecognition, { useSpeechRecognition } from 'react-speech-recognition';

import Siriwave from 'react-siriwave';
import TimeOut from "../Loading/TimeOut";

import detectBrowserLanguage from 'detect-browser-language';

const question_combo = [
    {
        link_ads: "/media/player1_croped.mp4",
        question: "How many spice in this video?",
        answer: [
            "1 spice",
            "2 spice",
            "3 spice"
        ],
        correct: "1 spice"
    },
    {
        link_ads: "/media/player2_croped.mp4",
        question: "What's name of this food?",
        answer: [
            "Kimchi",
            "Washabi",
            "Sauerkraut"
        ],
        correct: "Kimchi"
    },
    {
        link_ads: "/media/player3_croped.mp4",
        question: "What's name of brand?",
        answer: [
            "Dior",
            "Nike",
            "Gucchi"
        ],
        correct: "Dior"
    },
    {
        link_ads: "/media/player4_croped.mp4",
        question: "What's name of this food?",
        answer: [
            "Hamburger",
            "Fries",
            "Not Food"
        ],
        correct: "Hamburger"
    }
];


const PlayWithSpeech = () => {

    const {
        transcript,
        finalTranscript,
        resetTranscript,
    } = useSpeechRecognition();

    const userLanguage = detectBrowserLanguage();

    // if (!browserSupportsSpeechRecognition) {
    //     return <span>Browser doesn't support speech recognition.</span>;
    // }
    // const [loading, setLoading] = useState(true);

    const [amplitude, setAmplitude] = useState(0);

    const [isListening, setListening] = useState(false);

    const [playing, setPlaying] = useState(false);

    const [countdown, setCountdown] = useState(null);
    const [countdownStart, setCountdownStart] = useState(false);


    const [currentSlide, setCurrentSlide] = useState(0);
    const [currentQuestion, setCurrentQuestion] = useState(0);

    const [playerCurrentTime, setPlayerCurrentTime] = useState(0);
    const [playerDuration, setPlayerDuration] = useState(0);


    const [answerTimeOut, setAnswerTimeOut] = useState(false);

    const [yourAnswer, setYourAnswer] = useState(null);

    const [isCalculating, setCalculating] = useState(false);


    const [resultSummary, setResultSummary] = useState(0);

    //0 is incorrect, 1 is correct, -1 is not answer
    const [result, setResult] = useState(-1);

    const player = useRef();
    const slideTimeout = useRef(null);
    const questionController = useRef(null);

    const [playSoundCorrect] = useSound(
        '/media/mp3/Congrat.mp3',
    );

    const [playSoundIncorrect] = useSound(
        '/media/mp3/Wrong.mp3',
    );

    const [playSoundTictac] = useSound(
        '/media/mp3/Tictac.mp3',
    );

    const [playSoundStartQuestion] = useSound(
        '/media/mp3/Ringing.mp3',
    );

    const { speak } = useSpeechSynthesis();


    const initialDuration = () => {
        setPlayerDuration(player.current.getDuration() - 1);
    }

    const handlePlayVideo = () => {
        !playing ? setPlaying(true) : setPlaying(false);
    }

    const handlePauseVideo = () => {
        setPlaying(false);
    }

    const setTimeToAnswer = (timer) => {

        setListening(true);
        setCountdownStart(true);

        let counter = (timer / 1000) - 1;

        const count = setInterval(() => {
            if (counter < 0) {
                clearInterval(count);
            } else {
                setCountdown(counter);
                counter--;
            }
        }, 1000);

        setTimeout(() => {
            setListening(false);
            setCountdownStart(false);
            setAnswerTimeOut(true);
        }, timer);
    }

    const handleVideoEnded = () => {
        const splideData = slideTimeout.current.splide;
        splideData.go(currentQuestion + 1);
        speak({ text: question_combo[currentSlide].question })
    }

    const handleQuestion = (index) => {
        setCurrentQuestion(index);
    }

    const handleNextQuestion = (index) => {

        setCurrentSlide(index);
        setPlayerCurrentTime(0);
        setResult(-1);
        setAnswerTimeOut(false);
        setYourAnswer(null);
        setCountdown(null);
        setCountdownStart(false);
        setListening(false);
        resetTranscript();

        setTimeout(() => {
            setPlaying(true);
        }, 1500)

    }


    useEffect(() => {

        if (isListening) {
            SpeechRecognition.startListening({ continuous: true, language: userLanguage });
            console.log(transcript);
            setAmplitude(transcript.length / 2);
        }

    }, [
        isListening,
        amplitude,
        transcript,
        userLanguage
    ])

    useEffect(() => {
        if (finalTranscript && isListening) {
            if (finalTranscript.includes(1)) {
                setYourAnswer(question_combo[currentSlide].answer[0]);
            }

            if (finalTranscript.includes(2)) {
                setYourAnswer(question_combo[currentSlide].answer[1]);
            }

            if (finalTranscript.includes(3)) {
                setYourAnswer(question_combo[currentSlide].answer[2]);
            }
        }
    }, [
        finalTranscript,
        currentSlide,
        isListening
    ])

    useEffect(() => {
        const delayTimeToNextQuestion = (timer) => {
            const questionControllerData = questionController.current.splide;

            if (currentSlide + 1 > question_combo.length - 1) {
                console.log("END, NO QUESTION YET");

                setTimeout(() => {
                    setCalculating(true);
                }, 2000);

                return;
            }

            setTimeout(() => {
                setCurrentQuestion(0);
                questionControllerData.go(currentSlide + 1);
            }, timer)
        }


        const handleSubmitAnswer = () => {

            const trueAnswer = question_combo[currentSlide].correct;


            if (!yourAnswer) {
                setResult(-1);
                setAnswerTimeOut(false);
                delayTimeToNextQuestion(3000);
                return;
            }

            if (yourAnswer === trueAnswer) {
                setResult(1);
                setAnswerTimeOut(false);
                setResultSummary(resultSummary + 1);
                delayTimeToNextQuestion(3000);
                return;
            } else {
                setResult(0);
                setAnswerTimeOut(false);
                delayTimeToNextQuestion(3000);
                return;
            }
        }

        if (answerTimeOut) handleSubmitAnswer();


    }, [
        answerTimeOut,
        currentSlide,
        yourAnswer,
        resultSummary,
    ])

    const handleChoseAnswer = (answer) => {
        setYourAnswer(answer);
    }


    useEffect(() => {

        let getVideoTimer = null;

        if (player.current) {
            getVideoTimer = setInterval(() => {
                let currentTime = player.current.getCurrentTime();
                if (currentTime) {
                    setPlayerCurrentTime(currentTime);
                }
            }, 1000);
        }

        return function cleanup() {
            clearInterval(getVideoTimer);
        }

    }, [
        player,
    ]);

    useEffect(() => {
        if (result === 1) {
            playSoundCorrect();
        } else if (result === 0) {
            playSoundIncorrect();
        }
    }, [
        result,
        playSoundCorrect,
        playSoundIncorrect,
    ])

    useEffect(() => {

        if (currentSlide !== 0) playSoundStartQuestion();

    }, [
        currentSlide,
        playSoundStartQuestion
    ])

    useEffect(() => {
        if (countdownStart) playSoundTictac();
    }, [
        countdownStart,
        playSoundTictac
    ])


    return (
        <div className='bg-light_black h-screen relative flex flex-col items-center'>
            <LoadingStart />

            <ResultCalculating calculating={isCalculating} resultSummary={resultSummary} />

            <TimeOut timeout={answerTimeOut && !yourAnswer} />

            <div className="container flex flex-col h-full">
                <div className="w-full relative py-4">
                    <div className="flex justify-center">
                        <h1 className="text-white font-black text-30">1 ROUND</h1>
                    </div>
                    <Link to={"/welcome"} className="absolute right-0 top-1/2 transform -translate-y-1/2">
                        <div className='w-[20px] h-[20px]'>
                            <img src='/images/icons/cancel.svg' alt='' />
                        </div>
                    </Link>
                </div>

                <div className="flex flex-col relative w-full">


                    <Splide
                        ref={questionController}
                        options={{
                            rewind: false,
                            drag: false,
                            arrows: false,
                            pagination: true,
                            width: "100%",
                            height: "400px",
                            // gap: "30px"
                        }}

                        hasTrack={false}
                        onReady={() => { initialDuration(); }}
                        onMove={(splide) => { handleNextQuestion(splide.index); }}
                        onMoved={() => {

                            initialDuration();

                        }
                        }
                    >

                        <div className="w-full py-8 flex flex-col items-center">
                            <div className="bg-white w-[20px] h-[20px] flex items-center justify-center rounded-half mb-4">
                                <span>{currentSlide + 1}</span>
                            </div>
                            <ul className="splide__pagination splide__pagination--ltr" role={"tablist"} aria-label="Select a slide to show"></ul>
                        </div>

                        <SplideTrack>

                            {question_combo.map((item, index) => {
                                return (
                                    <SplideSlide key={index}>
                                        <div className="">
                                            <Splide
                                                ref={currentSlide === index ? slideTimeout : null}

                                                options={{
                                                    rewind: true,
                                                    arrows: true,
                                                    drag: false,
                                                    pagination: false,
                                                    width: "100%",
                                                    height: "400px",
                                                    gap: "30px"
                                                }}
                                                hasTrack={false}

                                                onMove={(splide) => handleQuestion(splide.index)}

                                                onMoved={() => {
                                                    setTimeToAnswer(10000);
                                                }
                                                }
                                            >
                                                <SplideTrack>

                                                    <SplideSlide>
                                                        <ProgressBar
                                                            completed={Math.round(playerCurrentTime * 100 / playerDuration)}
                                                            isLabelVisible={false}
                                                            width={"100%"}
                                                            height={"10px"}
                                                            bgColor={"linear-gradient(90deg, rgba(0,255,214,1) 0%, rgba(255,251,98,1) 52%, rgba(255,235,0,1) 80%, rgba(255,177,177,1) 100%)"}
                                                            borderRadius={"0"}
                                                        />
                                                        <div className="relative w-full bg-black h-full -z-2">
                                                            <div className="absolute xs:left-1/2 top-1/2 transform xs:-translate-x-1/2 -translate-y-1/2 -z-1">
                                                                <ReactPlayer
                                                                    ref={currentSlide === index ? player : null}
                                                                    key={index}
                                                                    url={item.link_ads}
                                                                    playing={playing && currentSlide === index}
                                                                    onReady={currentSlide === index ? initialDuration : () => { }}
                                                                    onEnded={handleVideoEnded}
                                                                    onPause={handlePauseVideo}
                                                                    config={{
                                                                        youtube: {
                                                                            playerVars: { showinfo: 0 }
                                                                        }
                                                                    }}
                                                                    width="100%"
                                                                    height="100%"
                                                                />
                                                            </div>
                                                        </div>


                                                        <div className="splide__arrows absolute bottom-[10%] right-[5%]">
                                                            <button id="previous_slide" className="splide__arrow splide__arrow--prev hidden">Prev</button>
                                                            <button id="next_slide" className="splide__arrow splide__arrow--next" onClick={handlePauseVideo}>
                                                                <div className='w-[50px] h-[50px] cursor-pointer bg-black bg-opacity-65 p-4 rounded-2'>
                                                                    <img src='/images/icons/arrow-right.svg' alt='' />
                                                                </div>
                                                            </button>
                                                        </div>
                                                    </SplideSlide>

                                                    <SplideSlide>
                                                        <div className="w-full">
                                                            <div className="">

                                                                {
                                                                    item.answer.map((answer_item, key) => {
                                                                        return (
                                                                            <div key={key} className="flex w-full mb-8 rounded-3 overflow-hidden cursor-pointer relative" onClick={() => handleChoseAnswer(answer_item)}>

                                                                                {
                                                                                    yourAnswer === answer_item ?
                                                                                        <div className="absolute w-full h-full bg-correct"></div>
                                                                                        : <></>
                                                                                }

                                                                                {result === -1 ?
                                                                                    <></>
                                                                                    : result && answer_item === item.correct ?
                                                                                        <div className="absolute w-full h-full bg-correct"></div>
                                                                                        : answer_item === item.correct ?
                                                                                            <div className="absolute w-full h-full bg-incorrect"></div>
                                                                                            : <></>
                                                                                }

                                                                                <div className="w-1/3 bg-[#4E4D4D] flex justify-center py-8">
                                                                                    <h3 className="text-26 text-white font-bold z-2"> {key + 1} 번 </h3>
                                                                                </div>
                                                                                <div className="w-2/3 bg-[#353434] flex items-center px-6">
                                                                                    <h3 className="text-24 text-white font-bold z-2"> {answer_item} </h3>
                                                                                </div>
                                                                            </div>
                                                                        );
                                                                    })
                                                                }
                                                            </div>
                                                        </div>
                                                    </SplideSlide>
                                                </SplideTrack>
                                            </Splide>
                                        </div>
                                    </SplideSlide>
                                );
                            })}

                        </SplideTrack>
                    </Splide>

                </div>
            </div>

            <div className="w-full container fixed bottom-0 py-10 ">

                <div className="flex flex-col justify-center items-center w-full">

                    {transcript ?
                        <h6 className="text-24 text-white font-bold z-2">" {transcript} "</h6>
                        : <></>
                    }


                    <div className="flex justify-center pb-2 pt-5">
                        {
                            countdown !== 0 ?
                                <h6 className="text-26 text-white font-bold z-2">{countdown}</h6>
                                : <></>
                        }
                    </div>



                    {
                        isListening ?
                            amplitude > 0 ?
                                <Siriwave
                                    amplitude={amplitude > 2 ? 2 : amplitude}
                                    style={"ios9"}
                                    width={290}
                                    height={100}
                                />
                                :
                                <Siriwave
                                    amplitude={.5}
                                    style={"ios9"}
                                    width={290}
                                    height={100}
                                />
                            : <></>
                    }

                    {
                        !isListening && finalTranscript ?
                            <div className='w-[60px] h-[60px] cursor-pointer'>

                                <img src='/images/icons/voice_done.png' alt='' />

                            </div>
                            : <></>
                    }
                </div>

                <div className="flex justify-between items-center ">
                    <div className='w-[45px] h-[45px] cursor-pointer'>
                        <img src='/images/icons/replay.svg' alt='' />
                    </div>

                    {currentQuestion === 0 ?
                        <div className='w-[50px] h-[50px] cursor-pointer' onClick={handlePlayVideo}>
                            {playing ?
                                <img src='/images/icons/pause.svg' alt='' />

                                : <img src='/images/icons/play.svg' alt='' />
                            }
                        </div>
                        : <></>
                    }

                    <div className='w-[45px] h-[45px] cursor-pointer'>
                        <img src='/images/icons/save.svg' alt='' />
                    </div>
                </div>
            </div>

        </div>
    )
}

export default PlayWithSpeech