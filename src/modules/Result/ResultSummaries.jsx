import React from "react";
import { Link } from "react-router-dom";

import useSound from 'use-sound';


const ResultSummaries = ({ resultSummary }) => {

    const [playEffect] = useSound(
        '/media/mp3/EndGame.mp3',
        { volume: 0.25 }
    );

    playEffect();

    return (
        <div className="w-full h-screen fixed bg-black bg-opacity-65 backdrop-blur-background z-1">
            <div className="absolute w-full h-full z-2">
                <iframe title="congrat_gif" src="https://giphy.com/embed/S9Lb4qT9puX3eoIC02" width="100%" height="100%" frameBorder="0" class="giphy-embed" allowFullScreen></iframe>
            </div>

            <div className="w-full h-full flex flex-col items-center justify-center">
                <div className="relative z-2 flex flex-col items-center">
                    <div className='w-[170px] h-[170px] transform -translate-x-[25%] translate-y-[75%] z-1'>
                        <img src='/images/icons/success.png' alt='' />
                    </div>
                    <div className='w-[180px] h-[180px] transform translate-x-[25%] z-2'>
                        <img src='/images/icons/like.png' alt='' />
                    </div>
                    <div className='w-[220px] h-[220px] transform translate-x-0 -translate-y-[40%] z-10'>
                        <img src='/images/icons/coin_dollar.png' alt='' />
                    </div>

                </div>

                <div className="flex items-center flex-col">
                    <h3 className='text-28 text-white font-black'>총 {resultSummary}문제 정답!</h3>
                    <h3 className='text-28 text-white font-black'>150원 획득!</h3>
                </div>

                <Link to={"/welcome"} className="w-fit bg-[#353434] flex items-center px-10 py-4 rounded-2 mt-10 cursor-pointer">
                    <h3 className="text-24 text-white font-bold z-2"> 다음 퀴즈 풀기 </h3>
                </Link>
            </div>

        </div>
    );

}

export default ResultSummaries