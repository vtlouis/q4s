import React from 'react';
import Header from './components/header/Header';

import { Link } from "react-router-dom";


function Welcome() {


  return (


    <div className='bg-light_black w-full h-screen'>
      <Header />
      <div className='w-full container my-8'>
        <div className='bg-blur_black w-fit py-2 px-4 rounded-6 flex items-center cursor-pointer'>
          <div className='w-[35px] h-[35px]'>
            <img src='/images/icons/coin.png' alt='' />
          </div>
          <h6 className='text-white font-bold text-32 pl-4 pr-8'>3250<strong className='font-normal text-24'> 원</strong> </h6>
          <div className='w-[18px] h-[18px]'>
            <img src='/images/icons/arrow-right.svg' alt='' />
          </div>
        </div>
      </div>


      <div className='w-full container'>
        <div className='flex items-center'>
          <div className='w-fit px-4'>
            <div className='w-[80px] h-[80px]'>
              <img src='/images/icons/cup.png' alt='' />
            </div>
          </div>

          <div className=''>
            <h6 className='text-24 font-medium text-white'>조금만 더 모으면 <strong className='font-bold'>커피</strong> 한 잔!</h6>
          </div>
        </div>
      </div>

      <div className='w-full container mt-12 mb-8 cursor-pointer'>
        <div className='flex items-center justify-center'>
          <div className='relative'>
            <div className='w-[290px] h-[290px] bg-start shadow-start rounded-half relative border-5 border-transparent circle'>

            </div>
            <Link to="/play">
              <div className='absolute top-1/2 left-1/2 transform -translate-x-1/2 -translate-y-1/2'>
                <div className='flex flex-col items-center justify-around'>
                  <h3 className='text-26 font-black text-light_black'>1 ROUND</h3>
                  <h3 className='text-26 font-black tracking-wide text-light_black py-4'>START!</h3>
                  <h6 className='text-14 text-light_black'>오늘도 달려볼까?</h6>
                </div>
              </div>

            </Link>

          </div>

        </div>
      </div>

    </div >

  );
}

export default Welcome;
