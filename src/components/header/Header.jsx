import React from "react"

const Header = () => {
    return (
        <div className='container'>
          <div className='w-full py-4 flex items-center justify-between'>
            <div className='flex items-center bg-clip-text bg-logo'>
              <h3 className='text-28 text-transparent font-bold tracking-tighter'>Quiz</h3>
              <div className='w-[30px] h-[30px]'>
                <img src='/images/icons/flash.png' alt='' />
              </div>
              <h3 className='text-28 text-transparent font-bold tracking-tighter'>Speed</h3>
            </div>
            <div className='flex items-center cursor-pointer'>
              <div className='w-[25px] h-[25px]'>
                <img src='/images/icons/save.svg' alt='' />
              </div>
              <div className='w-[30px] h-[30px] ml-4 cursor-pointer'>
                <img src='/images/icons/settings.svg' alt='' />
              </div>
            </div>
          </div>
        </div>
    )
}

export default Header