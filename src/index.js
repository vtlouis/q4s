import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import reportWebVitals from './reportWebVitals';
import Welcome from './Welcome';

import { BrowserRouter, Routes, Route } from "react-router-dom";
import ErrorPage from './modules/ErrorPage';
import Play from './modules/Play/Play';

export default function Quiz4Speed() {
  return (
    <React.StrictMode>
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<Welcome />}>
            <Route path="*" element={<ErrorPage />} />
          </Route>

          <Route path="/play" element={<Play />}>
            <Route path="*" element={<ErrorPage />} />
          </Route>

        </Routes>
      </BrowserRouter>
    </React.StrictMode>

  )
}

document.addEventListener('DOMContentLoaded', function (event) {
  const root = ReactDOM.createRoot(document.getElementById('root'));
  root.render(
    <Quiz4Speed />
  );
});



// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
